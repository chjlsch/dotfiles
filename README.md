```shell
ln -s $(pwd)/bash ~/.bashrc.d
ln -s $(pwd)/starship/starship.toml ~/.config/starship.toml
ln -s $(pwd)/helix/config.toml ~/.config/helix/config.toml
ln -s $(pwd)/helix/languages.toml ~/.config/helix/languages.toml
```
